# Vacuum Mass Flow & Gauge Controller MKS 946

EPICS module to provide communications and read/write data from/to MKS 946 vacuum mass flow & gauge controller

## Startup Examples

`iocsh -r vac_ctrl_mks946,catane -c 'requireSnippet(source-hvp.cmd, "DEVNAME=VEVMC-01100,CONNAME=mks946,ADDRESS=001,SLOTA=mfc-mks-gv50a,SLOTB=gauge-mks-vgd")'`
