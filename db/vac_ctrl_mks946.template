################################################################################
# Database for MKS Vacuum Mass Flow & Gauge Controller
# Common parameters
# Nick Levchenko - 2016-10-04
#
# Macro:
# DEVICENAME 	- Device name
# ASYNPORT		- Asyn PORT
#
################################################################################

record(bo, "$(DEVICENAME):LockS") {
    field(DESC, "Lock front panel")
    field(DTYP, "stream")
    field(OUT, "@vac_ctrl_mks946.proto lock(001) $(ASYNPORT)")
    field(ZNAM, "unlock")
    field(ONAM, "lock")
}

record(mbbi, "$(DEVICENAME):LockR") {
    field(DESC, "Get panel lock status")
    field(DTYP, "stream")
    field(INP,  "@vac_ctrl_mks946.proto get_lock(001) $(ASYNPORT)")
    field(ZRST, "unlock")
    field(ONST, "lock")
    field(PINI, "YES")
    field(SCAN, "1 second")
}

record(ai, "$(DEVICENAME):BaudrtR") {
    field(DESC, "Get serial connection baudrate")
    field(DTYP, "stream")
    field(INP,  "@vac_ctrl_mks946.proto get_baudrate(001) $(ASYNPORT)")
    field(PINI, "YES")
}

record(mbbo, "$(DEVICENAME):UnitS") {
    field(DESC, "Set pressure unit")
    field(DTYP, "stream")
    field(ZRST, "TORR")
    field(ONST, "MBAR")
    field(TWST, "PASCAL")
    field(THST, "MICRON")
    field(OUT,  "@vac_ctrl_mks946.proto set_unit(001) $(ASYNPORT)")
}

record(mbbi, "$(DEVICENAME):UnitR") {
    field(DESC, "Get pressure unit")
    field(DTYP, "stream")
    field(ZRST, "TORR")
    field(ONST, "MBAR")
    field(TWST, "PASCAL")
    field(THST, "MICRON")
    field(PINI, "YES")
    field(SCAN, "1 second")
    field(INP,  "@vac_ctrl_mks946.proto get_unit(001) $(ASYNPORT)")
}
