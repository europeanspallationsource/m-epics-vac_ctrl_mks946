# @field DEVNAME
# @type STRING
# MKS946's device name.

# @field CONNAME
# @type STRING
# Connection name to the MKS946's device.

# @field ADDRESS
# @type STRING
# Address parameter on the MKS946's device.

# @field SLOTA
# @type STRING
# MKS946's A slot device module name.

# @field SLOTB
# @type STRING
# MKS946's B slot device module name.

# @field SLOTC
# @type STRING
# MKS946's C slot device module name.

require vac_gauge_mks_vgd, 2.0.1-catania
require vac_mfc_mks_gv50a, 2.0.4-catania 

drvAsynIPPortConfigure("mks946", "10.10.1.21:4004")

dbLoadRecords("vac-$(SLOTA).db", "DEVNAME=$(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS), PORT1=1, PORT2=2")
dbLoadRecords("vac-$(SLOTB).db", "DEVNAME=$(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS), PORT1=3, PORT2=4")
dbLoadRecords("vac-$(SLOTC).db", "DEVNAME=$(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS), PORT1=5, PORT2=6")
dbLoadRecords("vac-ctrl-mks946.db", "DEVNAME = $(DEVNAME), CONNAME=$(CONNAME), ADDRESS=$(ADDRESS)")
