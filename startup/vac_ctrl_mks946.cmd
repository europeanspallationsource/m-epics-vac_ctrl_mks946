
drvAsynSerialPortConfigure("mks946", "/dev/ttyUSB0", 0, 0, 0)
asynSetOption("mks946", -1, "baud", "115200")
asynSetOption("mks946", -1, "bits", "8")
asynSetOption("mks946", -1, "parity", "none")
asynSetOption("mks946", -1, "stop", "2")
asynSetOption("mks946", -1, "clocal", "N")
asynSetOption("mks946", -1, "crtscts", "N")

dbLoadRecords("vac-ctrl-mks946.db")
